﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FleetPride.Framework.Excel
{
    public class BaseExcelModel
    {
        public bool IsValid { get; set; }
        public string ErrorMessage { get; set; }
    }
}
